# Presentation as a Service

A project to show a high-level end-to-end demo of the GitLab platform.

## Local dev

1. Install the [recommended VS Code workspace extensions](https://code.visualstudio.com/docs/editor/extension-marketplace#_workspace-recommended-extensions).
1. Run `npm install`.
1. Use the [Marp VS Code extension](https://marketplace.visualstudio.com/items?itemName=marp-team.marp-vscode) to preview your changes live.

### Building

Install the [Marp CLI](https://www.npmjs.com/package/@marp-team/marp-cli) with: `brew install marp-cli`

Then: **⇧⌘B** in VS Code. The output will be in the `build` folder.
